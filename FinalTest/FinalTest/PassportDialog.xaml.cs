﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FinalTest
{
    /// <summary>
    /// Interaction logic for PassportDialog.xaml
    /// </summary>
    public partial class PassportDialog : Window
    {
        Person currperson = new Person();

        public PassportDialog(Person person)
        {
            InitializeComponent();

            lblName.Content = person.PersonName;
            Person currperson = person;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            
                string tempPass = txtPassport.Text;
                string pattern = @"^[a-zA-Z]{2}\d{7}";
                Regex rg = new Regex(pattern);
                if (!rg.IsMatch(tempPass))
                {
                    MessageBox.Show("Passport must contain 2 Letters and 7 digits");
                return;
                }
            
           

            
            Passport passport = new Passport() { PassportId = currperson.PersonId, PassportNumber = tempPass };
            Global.context.Passports.Add(passport);
            Global.context.SaveChanges();
        }
    }
}
