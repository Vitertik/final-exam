﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FinalTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            lvAddPerson.ItemsSource = Global.context.PersonList.ToList();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            AddPerson addPerson = new AddPerson();
            addPerson.Show();
        }

        private void lvAddPerson_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvAddPerson.SelectedIndex == -1)
            {
                return;
            }
            Person p = (Person)lvAddPerson.SelectedItem;
            PassportDialog passportDialog = new PassportDialog(p);
            passportDialog.Show();
            

        }

        private void Window_Activated(object sender, EventArgs e)
        {
            lvAddPerson.ItemsSource = Global.context.PersonList.ToList();
        }
    }
}
