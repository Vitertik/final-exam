﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalTest
{
    public class Passport
    {
        [ForeignKey("Person")]
        public int PassportId { get; set; }
        public string PassportNumber { get; set; }

        public byte[] Photo { get; set; }

        public virtual Person Person { get; set; }
    }
}
