﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalTest
{
    class PersonDbContext : DbContext
    {
        const string DbName = "persondatabase2.mdf";
        static string DbPath = Path.Combine(Environment.CurrentDirectory, DbName);
        public PersonDbContext() : base($@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename={DbPath};Integrated Security=True;Connect Timeout=30")
        { }

        public DbSet<Person> PersonList { get; set; }

        public DbSet<Passport> Passports { get; set; }
    }
}
